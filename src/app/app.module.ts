import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { RouterModule,Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MemberlistComponent } from './MEMBERS/memberlist.component';
import { MembercreateComponent } from './MEMBERS/membercreate.component';
import { DisplaymemberComponent } from './MEMBERS/displaymember.component';
import { MemberDetailsComponent } from './MEMBERS/member-details.component';


import {Memberservice} from './MEMBERS/memberservice.service';

import {MemberfilterPipe} from './MEMBERS/memberFilter.pipe'
import { from } from 'rxjs';


const appRoutes : Routes=[
  {path: 'list',component:MemberlistComponent},
  {path: 'create',component:MembercreateComponent},
  {path: 'member/:Id',component:MemberDetailsComponent},
  {path:'edit/:Id',component:MembercreateComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    MemberlistComponent,
    MembercreateComponent,
    DisplaymemberComponent,
    MemberDetailsComponent,
    MemberfilterPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AppRoutingModule,
    FormsModule
  ],
  providers: [Memberservice],
  bootstrap: [AppComponent]
})
export class AppModule { }
