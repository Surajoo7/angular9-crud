export class Member{
    Id:number;
    Name:string;
    Age:number;
    DOB:Date;
    Gender:string;
    Email:string;
    Mobile:number;
    Isactive: boolean;
    Photopath:string

}