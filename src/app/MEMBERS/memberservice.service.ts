import{Injectable} from '@angular/core';
import{Member} from '../Models/Member.model';

@Injectable()
export class Memberservice{

    private listmember : Member[]=[
        {
            Id: 1,
            Name: "Suraj",
            Age:24,
            DOB: new Date(2/26/1996),
            Gender:"Male",
            Email:"smy@gmail.com",
            Mobile:1234567813,
            Isactive: true,
            Photopath:"./assets/images/1.jpg"
          },
          {
            Id: 2,
            Name: "Supriya",
            Age:26,
            DOB: new Date(3/17/1994),
            Gender:"Female",
            Email:"Nikamsup@gmail.com",
            Mobile:1234568913,
            Isactive: true,
            Photopath:"./assets/images/5.jpg"
          },
          {
            Id: 3,
            Name: "Ram",
            Age:22,
            DOB: new Date(5/21/1998),
            Gender:"Male",
            Email:"ram@gmail.com",
            Mobile:1854567813,
            Isactive: true,
            Photopath:"./assets/images/3.jpg"
          },
    ]; 
    private members :Member;

    getlistmember() : Member[]{
        return this.listmember;
    }

    getmemberbyid(id : number):Member{
      return this.listmember.find(m=>m.Id === id);
    }


    save(newmember:Member){
      if(newmember.Id==null){

        const newId= this.listmember.reduce(function(e1,e2){
          return(e1.Id > e2.Id) ? e1 : e2;
        }).Id;

        newmember.Id= newId+1;

        this.listmember.push(newmember);}
      else{
        const memberIndex= this.listmember.findIndex(e=> e.Id === newmember.Id);
        this.listmember[memberIndex]= newmember;
      }
    }

    delete(id:number){
        const dltId =this.listmember.findIndex(e=>e.Id === id)
        if(dltId != -1){
        this.listmember.splice(dltId,1);
        }
      }

      
      }