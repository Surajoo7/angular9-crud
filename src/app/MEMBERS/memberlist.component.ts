import { Component, OnInit } from '@angular/core';
import { Member } from '../Models/Member.model';
import {Memberservice} from './memberservice.service';
import{Router} from '@angular/router';

@Component({
  selector: 'app-memberlist',
  templateUrl: './memberlist.component.html',
  styleUrls: ['./memberlist.component.css']
})
export class MemberlistComponent implements OnInit {

   members : Member[];
   Searchname:string;
  // private nextmember=1;
   //MemberDisplay : Member;
  constructor(private Memberservicelist : Memberservice,
    private _router : Router) { }

  ngOnInit(): void {
    this.members= this.Memberservicelist.getlistmember();
//this.MemberDisplay=this.members[0];
  }

  /*nextMember(){
    if(this.nextmember <=2){
      this.MemberDisplay=this.members[this.nextmember];
      this.nextmember++;
    }
    else{
      this.MemberDisplay=this.members[0];
      this.nextmember=1;
    }
  }*/

  onClick(id:number){
    this._router.navigate(["/member",id]);
  }
}
