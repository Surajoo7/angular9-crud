import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router"
import { Memberservice } from './memberservice.service';
import { Member } from '../Models/Member.model';

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit {
  private id: number;
  mem: Member;
  constructor(private _router: ActivatedRoute,
    private _memberservice: Memberservice, private _route: Router) { }

  ngOnInit(): void {

    this._router.paramMap.subscribe(params => {
      this.id = +params.get('Id');
      this.mem = this._memberservice.getmemberbyid(this.id);
    });

  }

  nextMember() {
    if (this.id < 3) {
      this.id = this.id + 1;

    }
    else {
      this.id = 1;
    }

    this._route.navigate(['/member', this.id]);
  }

  editMember(){
    
    this._route.navigate(['/edit', this.id]);
  }

  deleteMember(){
    this._memberservice.delete(this.mem.Id);
    this._route.navigate(['list'])
  }

}
