import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Member} from '../Models/Member.model'; 
import {Memberservice} from './memberservice.service';
import { from } from 'rxjs';
import {ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-membercreate',
  templateUrl: './membercreate.component.html',
  styleUrls: ['./membercreate.component.css']
})
export class MembercreateComponent implements OnInit {

   PanelTitle : string;
  membercreate : Member;
    
    
   
  
    
  photopreview = false;
  constructor(private _route : Router,private _memberservice : Memberservice,
     private _Aroute:ActivatedRoute) { }

  ngOnInit(): void {
    this._Aroute.paramMap.subscribe(params => {
    const id = +params.get('Id');
    this.editthismember(id);
  });
}

private editthismember(id:number){
  if(id === 0){ 
    this.membercreate={
      Id: null,
      Name: null,
      Age:null,
      DOB: null,
      Gender:null,
      Email:'',
      Mobile:null,
      Isactive: null,
      Photopath:null
  };
  this.PanelTitle ="Create new Member";
  }
  else{
    this.PanelTitle ="Edit Member";
    this.membercreate=this._memberservice.getmemberbyid(id);
  }
}

 
  createMember():void{
    this._memberservice.save(this.membercreate);
    this._route.navigate(['list']);
  }

  preview(){
    this.photopreview= !this.photopreview;
  }

}
