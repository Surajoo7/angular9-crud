import { PipeTransform, Pipe } from '@angular/core';
import { Member } from '../Models/Member.model';


@Pipe({

    name:'memberFilter'
})
export class MemberfilterPipe implements PipeTransform{

    transform(members:Member[], Searchname:string):Member[]{
        if(!members || !Searchname){
            return members;
        }
        else{
            return members.filter(members=>
                 members.Name.toLowerCase().indexOf(Searchname.toLowerCase()) !== -1);
        }
    }
}