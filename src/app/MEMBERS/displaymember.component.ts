import { Component, OnInit,Input } from '@angular/core';
import { Member } from '../Models/Member.model';

@Component({
  selector: 'app-displaymember',
  templateUrl: './displaymember.component.html',
  styleUrls: ['./displaymember.component.css']
})
export class DisplaymemberComponent implements OnInit {

  @Input() mem :Member;
 
  constructor() { }

  ngOnInit(): void {
  }

}
